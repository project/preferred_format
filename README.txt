
PREFERRED FORMAT
================

Introduction
------------
This module allows your users to set their preferred default input format for placing comments and each content type.

As the site administrator you can choose which roles are allowed to choose their preferred default input on the permissions page.

Installation
------------
1. Copy the preferred_format folder to your sites/all/modules directory.
2. At Administer -> User Management -> Permissions (admin/user/permissions) select which roles should be able to choose their own input format.

Features
--------
- Define which roles can choose their preferred input format.
- Users who are allowed to choose their preferred input format can do so for comment forms and specify which input format to use for each content type they're allowed to create content for.

Issues / Feature requests
-------------------------
If you find a bug, or have a feature request, please go to :

http://drupal.org/project/issues/preferred_format

Contact
-------
This module is being maintained by me, Geoffrey de Vlugt (gdevlugt). 

If you need to contact me, use the contact form at :

http://drupal.org/user/167273/contact